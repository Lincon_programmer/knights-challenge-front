import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import HomeView from "../views/HomeView.vue";
import CreateKnight from "../views/CreateKnight.vue";
import KnightDetails from "../views/KnightDetails.vue";
import KnightList from "../views/KnightList.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/create-knight",
    name: "CreateKnight",
    component: CreateKnight,
  },
  {
    path: "/knight/:id",
    name: "KnightDetails",
    component: KnightDetails,
    props: true,
  },
  {
    path: "/knights",
    name: "KnightList",
    component: KnightList,
  },
  {
    path: "/Heroes",
    name: "HeroesList",
    component: () => import("../views/HeroesList.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
